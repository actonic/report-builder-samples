# Bar chart with right legend and designations


A bar chart or bar graph is a chart or graph that presents categorical data with rectangular bars with heights or lengths proportional to the values that they represent. In this case, the legend is displayed on the right and designations are displayed on the columns.

![bar-chart-with-right-legend-and-designations](./bar-chart-with-right-legend-and-designations.png "Example view")

[bar-chart-with-right-legend-and-designations.js](./bar-chart-with-right-legend-and-designations.js)

Don't forget to update template:  

```
<canvas id="bar-chart-with-right-legend-and-designations" width="800" height="400"></canvas>
```
