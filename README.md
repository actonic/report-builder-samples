# Report Builder #

Scripted Reports of the app entail a comprehensive reporting framework, helping Jira users to create reports using their favorite visualization libraries, without developing their own app. Users are able to create ANY report for Jira Core, Software and even Service Desk.

## About the app ##
* [Atlassian Marketplace](https://marketplace.atlassian.com/apps/1216997/report-builder?tab=overview)
* [Scripted Report module documentation](https://actonic.atlassian.net/wiki/spaces/ARB/pages/944406529/Scripted+Reports)
* [How to use reports from the repository](https://actonic.atlassian.net/wiki/spaces/ARB/pages/1114341377/Quick+Start+Guide+On+How+To+Build+A+Report)

# Examples #

## Line Chart ##

### Cumulative flow diagram

![cumulative-flow-diagram](line-charts/cumulative-flow-diagram.png "Example view")

[cumulative-flow-diagram.md](line-charts/cumulative-flow-diagram.md)

## Bar Chart ##

### Bar chart with right legend and designations

![bar-chart-with-right-legend-and-designations](bar-charts/bar-chart-with-right-legend-and-designations.png "Example view")

[bar-chart-with-right-legend-and-designations.md](bar-charts/bar-chart-with-right-legend-and-designations.md)

## Pie Chart ##

### Pie chart with right legend

![pie-chart-with-right-legend](pie-charts/pie-chart-with-right-legend.png "Example view")

[pie-chart-with-right-legend.md](pie-charts/pie-chart-with-right-legend.md)

## Contribution guidelines ##

* Code review
* Other guidelines

## Any questions? Need support? ##

* Issue tracker: https://actonic.atlassian.net/servicedesk/customer/portal/8
* Other community or team contact