/* 
bar-chart-with-right-legend-and-designations.js
Don't forget to add:
    <canvas id="bar-chart-with-right-legend-and-designations" width="800" height="400"></canvas>
to "Template".
*/
const {
    moment
} = SR.lib;

class SampleData {
    JIRA_CORE_STATUSES = [
        'Done', 'Open', 'In Progress', 'To Do', 'In Review', 'Under review', 'Approved', 'Cancelled', 'Rejected', 'Draft',
        'Published', 'Interviewing', 'Interview Debrief', 'Screening', 'Offer Discussions', 'Accepted', 'Applications', 'Second Review',
        'Lost', 'Won', 'Contacted', 'Opportunity', 'In Negotiation', 'Purchased', 'Requested'
    ]
    COLORS = [
        'rgb(94, 184, 77)', // green
        'rgb(111, 204, 221)', // light-blue
        'rgb(246, 112, 25)', // orange
        'rgb(146, 190, 210)', // dark-blue
        'rgb(117, 83, 158)', // purple 
        'rgb(172, 194, 54)', // lime
        'rgb(88, 89, 91)' // gray
    ]
    COLORS_TRANSPERENT = [
        'rgb(94, 184, 77, 0.1)', // green
        'rgb(111, 204, 221, 0.1)', // light-blue
        'rgb(246, 112, 25, 0.1)', // orange
        'rgb(146, 190, 210, 0.1)', // dark-blue
        'rgb(117, 83, 158, 0.1)', // purple 
        'rgb(172, 194, 54, 0.1)', // lime
        'rgb(88, 89, 91, 0.1)' // gray
    ]
    static MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]
    getRisingDates(count, startFromDate) {
        let startDate = startFromDate ? startFromDate : Date.now();
        let resultArray = Array.from({
            length: count
        }, (v, i) => moment(startDate).add(i, 'day').format('D MMM'));
        return resultArray;
    }
    getRandRisingNumbers(count, startFromValue) {
        let startValue = startFromValue ? startFromValue : 0;
        let resultArray = Array.from({
            length: count
        }, (v, i) => startValue + i * this.getRandomInt(1, 3));
        return resultArray;
    }
    getRandNumbers(count, initialMinValue, initialMaxValue) {
        let minValue = initialMinValue && initialMinValue > 0 ? initialMinValue : 0;
        let maxValue = initialMaxValue && initialMaxValue > 1 ? initialMaxValue : 10;
        let resultArray = Array.from({
            length: count
        }, (v, i) => this.getRandomInt(minValue, maxValue));
        return resultArray;
    }
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }
}

const sampleData = new SampleData();
const configLabels = sampleData.getRisingDates(4);
const configFirstData = sampleData.getRandNumbers(4);
const configSecondData = sampleData.getRandNumbers(4);
const configThirdData = sampleData.getRandNumbers(4);

const config = {
    type: 'bar',
    data: {
        labels: configLabels,
        datasets: [{
                label: sampleData.JIRA_CORE_STATUSES[0],
                data: configFirstData,
                backgroundColor: sampleData.COLORS[0],
            },
            {
                label: sampleData.JIRA_CORE_STATUSES[1],
                data: configSecondData,
                backgroundColor: sampleData.COLORS[1],
            },
            {
                label: sampleData.JIRA_CORE_STATUSES[2],
                data: configThirdData,
                backgroundColor: sampleData.COLORS[2],
            },
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'right',
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                },
                stacked: true
            }],
            yAxes: [{
                gridLines: {
                    display: false,
                },
                ticks: {
                    display: false,
                },
                stacked: true
            }]
        }
    },
    plugins: {
        afterDatasetsDraw: function(chart, easing) {
            const ctx = chart.chart.ctx;
            var fontSize = 17;
            var fontFamily = Chart.defaults.global.defaultFontFamily;
            ctx.font = Chart.helpers.fontString(fontSize, 'normal', fontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';

            chart.data.datasets.forEach(function(dataset, i) {
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        let currentData;
                        switch (i) {
                            case 0:
                                currentData = configFirstData
                                break;
                            case 1:
                                currentData = configSecondData
                                break;
                            default:
                                currentData = configThirdData
                        }
                        var position = element.tooltipPosition();
                        ctx.fillStyle = '#fff';
                        if(currentData[index] !== 0) {
                            ctx.fillText(currentData[index].toString(), position.x, position.y + element.height() / 2);
                        } else {
                            ctx.fillText(currentData[index].toString(), position.x, position.y);
                        }
                    });
                }
            });
            ctx.save();
            ctx.restore();
        },
    }
}

SR.render({}, () => {
    new Chart(document.getElementById('bar-chart-with-right-legend-and-designations'), config);
});