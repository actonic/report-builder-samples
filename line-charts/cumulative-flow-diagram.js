/* 
cumulative-flow-diagram.js
Don't forget to add:
    <canvas id="cumulative-flow-diagram" width="800" height="400"></canvas>
to "Template".
*/
const {
    moment
} = SR.lib;

class SampleData {
    JIRA_CORE_STATUSES = [
        'Done', 'Open', 'In Progress', 'To Do', 'In Review', 'Under review', 'Approved', 'Cancelled', 'Rejected', 'Draft',
        'Published', 'Interviewing', 'Interview Debrief', 'Screening', 'Offer Discussions', 'Accepted', 'Applications', 'Second Review',
        'Lost', 'Won', 'Contacted', 'Opportunity', 'In Negotiation', 'Purchased', 'Requested'
    ]
    COLORS = [
        'rgb(94, 184, 77)', // green
        'rgb(111, 204, 221)', // light-blue
        'rgb(246, 112, 25)', // orange
        'rgb(146, 190, 210)', // dark-blue
        'rgb(117, 83, 158)', // purple 
        'rgb(172, 194, 54)', // lime
        'rgb(88, 89, 91)' // gray
    ]
    COLORS_TRANSPERENT = [
        'rgb(94, 184, 77, 0.1)', // green
        'rgb(111, 204, 221, 0.1)', // light-blue
        'rgb(246, 112, 25, 0.1)', // orange
        'rgb(146, 190, 210, 0.1)', // dark-blue
        'rgb(117, 83, 158, 0.1)', // purple 
        'rgb(172, 194, 54, 0.1)', // lime
        'rgb(88, 89, 91, 0.1)' // gray
    ]
    static MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]
    getRisingDates(count, startFromDate) {
        let startDate = startFromDate ? startFromDate : Date.now();
        let resultArray = Array.from({
            length: count
        }, (v, i) => moment(startDate).add(i, 'day').format('D MMM'));
        return resultArray;
    }
    getRandRisingNumbers(count, startFromValue) {
        let startValue = startFromValue ? startFromValue : 0;
        let resultArray = Array.from({
            length: count
        }, (v, i) => startValue + i * this.getRandomInt(1, 3));
        return resultArray;
    }
    getRandNumbers(count, initialMinValue, initialMaxValue) {
        let minValue = initialMinValue && initialMinValue > 0 ? initialMinValue : 0;
        let maxValue = initialMaxValue && initialMaxValue > 1 ? initialMaxValue : 10;
        let resultArray = Array.from({
            length: count
        }, (v, i) => this.getRandomInt(minValue, maxValue));
        return resultArray;
    }
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }
}

const sampleData = new SampleData();
const labels = sampleData.getRisingDates(15);
const datasets = [{
    fill: true,
    label: sampleData.JIRA_CORE_STATUSES[0],
    backgroundColor: sampleData.COLORS_TRANSPERENT[0],
    borderColor: sampleData.COLORS[0],
    data: sampleData.getRandRisingNumbers(15),

}, {
    fill: true,
    label: sampleData.JIRA_CORE_STATUSES[1],
    backgroundColor: sampleData.COLORS_TRANSPERENT[1],
    borderColor: sampleData.COLORS[1],
    data: sampleData.getRandRisingNumbers(15),
}, {
    fill: true,
    label: sampleData.JIRA_CORE_STATUSES[2],
    backgroundColor: sampleData.COLORS_TRANSPERENT[2],
    borderColor: sampleData.COLORS[2],
    data: sampleData.getRandRisingNumbers(15),
}, {
    fill: true,
    label: sampleData.JIRA_CORE_STATUSES[3],
    backgroundColor: sampleData.COLORS_TRANSPERENT[3],
    borderColor: sampleData.COLORS[3],
    data: sampleData.getRandRisingNumbers(15),
}];

const config = {
    type: 'line',
    data: {
        labels: labels,
        datasets: [...datasets]
    },
    options: {
        responsive: true,
        scales: {
            yAxes: [{
                stacked: true,
            }]
        },
        interaction: {
            mode: 'nearest',
            axis: 'x',
            intersect: false
        },
        plugins: {
            tooltip: {
                position: 'average'
            }
        }
    }
}

SR.render({}, () => {
    window.myChart = new Chart(document.getElementById('cumulative-flow-diagram'), config);
});