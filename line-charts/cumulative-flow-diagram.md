# Cumulative flow diagram

A cumulative flow diagram is a tool used in queuing theory. It is an area graph that depicts the quantity of work in a given state, showing arrivals, time in queue, quantity in queue, and departure. Cumulative flow diagrams are seen in the literature of agile software development and lean product development.

![cumulative-flow-diagram](./cumulative-flow-diagram.png "Example view")

[cumulative-flow-diagram.js](./cumulative-flow-diagram.js)

Don't forget to update template:  

```
<canvas id="cumulative-flow-diagram" width="800" height="400"></canvas>
```
