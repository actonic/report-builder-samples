/* 
pie-chart-with-right-legend.js
Don't forget to add:
    <canvas id="pie-chart-with-right-legend" width="800" height="400"></canvas>
to "Template".
*/
const {
    moment
} = SR.lib;

class SampleData {
    JIRA_CORE_STATUSES = [
        'Done', 'Open', 'In Progress', 'To Do', 'In Review', 'Under review', 'Approved', 'Cancelled', 'Rejected', 'Draft',
        'Published', 'Interviewing', 'Interview Debrief', 'Screening', 'Offer Discussions', 'Accepted', 'Applications', 'Second Review',
        'Lost', 'Won', 'Contacted', 'Opportunity', 'In Negotiation', 'Purchased', 'Requested'
    ]
    COLORS = [
        'rgb(94, 184, 77)', // green
        'rgb(111, 204, 221)', // light-blue
        'rgb(246, 112, 25)', // orange
        'rgb(146, 190, 210)', // dark-blue
        'rgb(117, 83, 158)', // purple 
        'rgb(172, 194, 54)', // lime
        'rgb(88, 89, 91)' // gray
    ]
    COLORS_TRANSPERENT = [
        'rgb(94, 184, 77, 0.1)', // green
        'rgb(111, 204, 221, 0.1)', // light-blue
        'rgb(246, 112, 25, 0.1)', // orange
        'rgb(146, 190, 210, 0.1)', // dark-blue
        'rgb(117, 83, 158, 0.1)', // purple 
        'rgb(172, 194, 54, 0.1)', // lime
        'rgb(88, 89, 91, 0.1)' // gray
    ]
    static MONTHS = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]
    getRisingDates(count, startFromDate) {
        let startDate = startFromDate ? startFromDate : Date.now();
        let resultArray = Array.from({
            length: count
        }, (v, i) => moment(startDate).add(i, 'day').format('D MMM'));
        return resultArray;
    }
    getRandRisingNumbers(count, startFromValue) {
        let startValue = startFromValue ? startFromValue : 0;
        let resultArray = Array.from({
            length: count
        }, (v, i) => startValue + i * this.getRandomInt(1, 3));
        return resultArray;
    }
    getRandNumbers(count, initialMinValue, initialMaxValue) {
        let minValue = initialMinValue && initialMinValue > 0 ? initialMinValue : 0;
        let maxValue = initialMaxValue && initialMaxValue > 1 ? initialMaxValue : 10;
        let resultArray = Array.from({
            length: count
        }, (v, i) => this.getRandomInt(minValue, maxValue));
        return resultArray;
    }
    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
    }
}

const sampleData = new SampleData();
const configLabels = sampleData.getRisingDates(5);
const configData = sampleData.getRandNumbers(5);
const configColors = [sampleData.COLORS[0], sampleData.COLORS[1], sampleData.COLORS[2], sampleData.COLORS[3], sampleData.COLORS[4]];

const config = {
    type: 'pie',
    data: {
        labels: configLabels,
        datasets: [{
            data: configData,
            backgroundColor: configColors,
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            position: 'right',
        },
    },
    plugins: {
        afterDatasetsDraw: function(chart, easing) {
            const ctx = chart.chart.ctx;
            var fontSize = 17;
            var fontFamily = Chart.defaults.global.defaultFontFamily;
            ctx.font = Chart.helpers.fontString(fontSize, 'normal', fontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';

            chart.data.datasets.forEach(function(dataset, i) {
                var meta = chart.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        if(configData[index] !== 0) {
                            var position = element.tooltipPosition();
                            ctx.fillStyle = '#fff';
                            ctx.fillText(configData[index].toString(), position.x, position.y);
                        }
                    });
                }
            });
            ctx.save();
            ctx.restore();
        },
    }
}

SR.render({}, () => {
    new Chart(document.getElementById('pie-chart-with-right-legend'), config);
});