# Pie chart with right legend


A pie chart is a circular statistical graphic, which is divided into slices to illustrate numerical proportion. In a pie chart, the arc length of each slice, is proportional to the quantity it represents. In this case, the legend is displayed on the right.

![pie-chart-with-right-legend](./pie-chart-with-right-legend.png "Example view")

[pie-chart-with-right-legend.js](./pie-chart-with-right-legend.js)

Don't forget to update template:  

```
<canvas id="pie-chart-with-right-legend" width="800" height="400"></canvas>
```
